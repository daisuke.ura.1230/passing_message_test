#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Imu 

if __name__ == "__main__":
    rospy.init_node("zengo_pub")

    rate = rospy.Rate(1000)

    publisher = rospy.Publisher("test", Imu, queue_size=1)

    while not rospy.is_shutdown():
        imu = Imu()
        imu.header.stamp = rospy.Time.now()

        publisher.publish(imu)
        rate.sleep()
