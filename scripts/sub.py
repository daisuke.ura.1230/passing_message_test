#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Imu 

prev_sec = 0


def callback(data):
    global prev_sec
    sec = data.header.stamp.to_sec()
    if sec < prev_sec:
        rospy.loginfo(sec - prev_sec)
    prev_sec = sec


if __name__ == "__main__":
    rospy.init_node("zengo_sub")

    subscriber = rospy.Subscriber("test", Imu, callback)

    rate = rospy.Rate(0.2)

    rospy.loginfo("start")
    while not rospy.is_shutdown():
        rospy.loginfo("waiting....")
        rate.sleep()
